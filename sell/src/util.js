/**
 * Created by Luby on 2018/5/29.
 */

// 工具函数库
import confit from './config'
export function get(url) {
  return new Promise((resolver, reject) => {
    // wx.request({
    //   url: confit.host + url,
    //   success: function (res) {
    //     if (res.data.code === 0) {
    //       resolver(res.data)
    //     } else {
    //       reject(res.data)
    //     }
    //   }
    // })
  })
}

export function showToast(text) {
  wx.showToast({
    title: text,
    icon: 'success',
    duration: 2000
  })
}



export function setStorageSync(key,value) {
   wx.setStorageSync(key, value)
}
export function getStorageSync(key) {
   return wx.getStorageSync(key)
}
//扫码
export function scanCode(callback) {
  return new Promise((resolver, reject) => {
    wx.scanCode({
      success: (res) => {
        resolver(res);
      }
    })
  })
}
