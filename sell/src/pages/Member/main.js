import Vue from 'vue'
import App from './Member'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '成员详情'
  }
}
