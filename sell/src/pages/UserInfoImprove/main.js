import Vue from 'vue'
import App from './UserInfoImprove'

const app = new Vue(App)
app.$mount()


export default {
  config: {
    navigationBarTitleText: '完善信息',
    disableScroll: true
  }
}
