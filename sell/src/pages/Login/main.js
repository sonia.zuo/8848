import Vue from 'vue'
import App from './Login'

const app = new Vue(App)
app.$mount()


export default {
  config: {
    navigationBarTitleText: '登录',
    disableScroll: true
  }
}
