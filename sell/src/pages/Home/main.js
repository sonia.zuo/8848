import Vue from 'vue'
import App from './Home'

const app = new Vue(App)
app.$mount()


export default {
  config: {
    navigationBarTitleText: '首页',
    "enablePullDownRefresh": false,
    // "disableScroll": true
    // disableScroll: true
  }
}
