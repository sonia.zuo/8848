import Vue from 'vue'
import App from './App'
Vue.config.productionTip = false;
App.mpType = 'app'

const app = new Vue(App);
var mta= require('./utils/mta_analysis.js');
import { VeevlinkOssUrl } from './utils/base'

// 如果OSS是正式的 则 埋点配置也要是正式的
if(VeevlinkOssUrl() === 'https://ebensz-sales.oss-cn-hangzhou.aliyuncs.com'){
  console.log('是正式埋点');
  mta.App.init({
    "appID":"500632591",
    "eventID":"500632592",
    "statPullDownFresh":true,
    "statShareApp":true,
    "statReachBottom":true
  });
}else{
  console.log('是测试埋点');
  mta.App.init(
    {
      "appID":"500630296",
      "eventID":"88481001"
    }
  );
}

app.$mount();

export default {
  // 这个字段走 app.json  classchange
  config: {
    // 页面前带有 ^ 符号的，会被编译成首页，其他页面可以选填，我们会自动把 webpack entry 里面的入口页面加进去
    pages: ['pages/Home/main', '^pages/Sign/main'],
    window: {
      backgroundTextStyle: 'light',
      navigationBarBackgroundColor: '#333',
      navigationBarTitleText: '8848售后服务平台',
      navigationBarTextStyle: '#fff'
    },
    // "tabBar":{
    //   selectedColor : '#212121',
    //   list:[
    //   ]
    // }
  }
}



