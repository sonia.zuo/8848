/**
 * Created by Luby on 2018/5/31.
 */
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    CardAdd:{},
    CardCancel:{},
    CardInfo:{},
    CardList:{},
    CardListUser:{},
    CardQuery:{},
    Condition:{},
    Confirm:{},
    Group:{},
    GroupAdd:{},
    GroupUser:{},
    GroupUserInfo:{},
    Home:{},
    Login:{},
    UserAdd:{},
    Member:{},
    UserInfoImprove:{}
  },
  mutations: {
    add: (state,array) => {
      console.log(array);
      const obj = state
      obj[array[0]][array[1]] = array[2];
    }
  }
})

export default store
